# Pre-Requisites
Setup `gcloud` and set the project to use.
```
gcloud auth login
gcloud config set project <project-id>
```
GCP project should have `container.googleapis.com` and `cloudbuild.googleapis.com` services enabled. This can be done by:
```
gcloud services enable container.googleapis.com
gcloud services enable cloudbuild.googleapis.com
```

# Docker Images
Kubernetes is a container orchestration tool. Manifest files make use of container which in turn need to have docker images built and accessible.  

## Code Repositories
Clone repositories that contain the updated code in the targeted branch and contains the Dockerfile to build the images.
```
git clone <frontend-repo> .
git clone <django-repo> .
git clone <ml-repo> .
```
**`frontend-repo`** is the URL to clone the frontend repository from.  
**`django-repo`** is the URL to clone the django repository from.  
**`ml-repo`** is the URL to clone the ML repository from.  

## Application Images
Each command below must be executed inside the directory that contains the code as well as the `Dockerfile` for desired image.  
```
gcloud builds submit . \
  -t gcr.io/<project-id>/django:<version>

gcloud builds submit . \
  -t gcr.io/<project-id>/frontend:<version>

gcloud builds submit . \
  -t gcr.io/<project-id>/audio:<version>

gcloud builds submit . \
  -t gcr.io/<project-id>/overlay:<version>

gcloud builds submit . \
  -t gcr.io/<project-id>/underlay:<version>
```
**`project-id`** is the GCP project id that will container the docker images in GCR service.  
**`version`** is the tag to give to docker image.  

# Kubernetes Cluster
Create a GKE cluster by typing:
```
name=<cluster-name> regionName=<region-name> CIDRS=<cidr1>,<cidr2> \
  machineType=<gcp-machine-type> min=<min-node-num> max=<max-node-num> \
  bash helper.sh createCluster
```
**`name`** is the name of GKE cluster you want to create.  
**`regionName`** is the region name where cluster will reside. This will make your cluster regional.  
**`CIDRS`** is a comma-separated list of CIDR to whitelist for master which is used by `kubectl` to connect to the cluster. If not provided, script will try to detect your external IP and make a CIDR out of it.  
**`machineType`** is type of machine for cluster nodes. Default to `n1-standard-1`.  
**`min`** and **`max`** represent the min and max number of nodes in cluster.  

> **Note:** Instead of making your cluster regional, you can specify `zoneName=<zone-name>` to make it zonal only. Regional cluster will have one node in each zone of the region while a zonal cluster will have one node only in specified zone.

## Update Cluster
You can update the cluster to whitelist different CIDR addresses by typing:
```
name=<cluster-name> regionName=<region-name> CIDRS=<cidr1>,<cidr2> bash helper.sh updateCluster
```
> **Note:** Just like cluster creation, you can use `zoneName` instead of `regionName` if your cluster is zonal.

## Configure Kubectl
Once your cluster has been created on GCP, you can use `kubectl` to deploy and debug the application. First, you need to install the tool by using:
```
sudo apt-get install kubectl
```
Now you need to set cluster credentials in `kubectl`, which can be done by using:
```
gcloud container clusters get-credentials <cluster-name> (--region <regiona-name> | --zone <zone-name>)
```
**`cluster-name`** is the name of K8 cluster you want to configure `kubectl` against.  
**`region-name`** is the name of GCP region, in case of regional cluster, where your K8 cluster resides in.  
**`zone-name`** is the name of GCP zone, in case of zonal cluster, where your K8 clsuter resides in.  

## ConfigMaps
Create configMap to create config values.
```
sed -e "s/\$BUCKET_NAME/<gcs-bucket>/g" \
  config.yaml | kubectl apply -f -
```  
**`gcs-bucket`** is the name of GCS bucket that will be mounted in containers.  
  
Create configMap to create config values.
```
kubectl create configmap config-files \
  --from-file frontend/default.conf
```
Create configMap to store scripts.
```
kubectl create configmap scripts \
  --from-file configure-gcsfuse.sh
```
To list down available configMaps on K8 cluster.
```
kubectl get configmap
```
To get details on a specific configmap.
```
kubectl describe configmap <configmap-name>
```

## Secrets
Create secret resource on K8 cluster. These are used to save critical information in encrypted format on the cluster.  

### GCP Service Account
Create a service account to use in K8 cluster as a secret resource. Service Account should have the following roles in GCP which can be done either at the time of creation or by editing it on IAM section.  
  
1. **`Text-to-Speech API`** – To interact with Google API for text & speech.  
2. **`Cloud SQL Client`** – To make connection with Cloud SQL instance.  
3. **`Storage Admin`** – To read/write files fom cloud storage using GCSFuse.  
  
  
To create service account on GCP using `gcloud`, run the following.
```
gcloud iam service-accounts create k8-application \
  --display-name 'K8 Application Cluster Service Account'
```
To add roles to the service account, run the following.
```
PROJECT_ID=$(gcloud config get-value project)

gcloud projects add-iam-policy-binding $PROJECT_ID \
  --member serviceAccount:k8-application@${PROJECT_ID}.iam.gserviceaccount.com \
  --role roles/cloudsql.client

gcloud projects add-iam-policy-binding $PROJECT_ID \
  --member serviceAccount:k8-application@${PROJECT_ID}.iam.gserviceaccount.com \
  --role roles/storage.admin

# One more role should be here that gives access to Google Text-Speech API.
```
To download service account key, run the following.
```
PROJECT_ID=$(gcloud config get-value project)
gcloud iam service-accounts keys create <output-name> \
  --iam-account k8-application@${PROJECT_ID}.iam.gserviceaccount.com
```
**`output-name`** is the name of to-be-download JSON key.  
  
Now that you have service account key on your system, run the following to create K8 secret resource.  
```
kubectl create secret generic google-cloud \
  --from-file key.json=<path-to-key.json>
```
**`path-to-key.json`** is the path to JSON key file of GCP service account.  
  
To list down available secrets on K8 cluster.
```
kubectl get secrets
```
To get details on a specific secret.
```
kubectl describe secrets <secret-name>
```

# Application Deployment
You need to know about GCP project and docker image tag version to use in K8 manifest file. To list tags of docker images stored in GCR service of GCP, you can run.
```
gcloud container images list-tags gcr.io/<project-id>/<image-name>
```
**`project-id`** is the GCP project id that contains the docker image in GCR service.  
**`image-name`** is the docker image name you want to list the tags against.  
  
## Kubernetes Resources
To check the deployments on K8 cluster.
```
kubectl get deployments
```
To check the pods and their logs on K8 cluster.
```
# List available pods.
kubectl get pods

# Get logs from a pod.
kubectl logs <pod-name>
```
To check the services on K8 cluster.
```
kubectl get svc
```
To check the cronjobs on K8 cluster.
```
kubectl get cronjobs
```
Using `kubectl`, you can achieve everything that is feasible from kubernetes API. Here is a [link to kubectl cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/) to achieve common operations.  

## Deploy ProxySQL and Redis
To deploy proxysql container that will be used by django for database connection.
```
sed -e "s/\$PROJECT/<project-id>/g" \
  -e "s/\$REGION/<region>/g" \
  -e "s/\$INSTANCE/<instance>/g" \
  django/proxysql.yaml | kubectl apply -f -
```
**`project-id`** is the GCP project id that contains the docker image in GCR service.  
**`region`** is the GCP region where Cloud SQL instance resides in.  
**`instance`** is the name of Cloud SQL instance to connect to.  
  
To deploy redis container that is used as message broker for celery, run the following.
```
kubectl apply -f redis/redis.yaml
```

## Before building the frontend image

Ensure that the correct configurations for the frontend app are made under the following
base settings.

```
nfluence-alpha-ui/src/webapi/webapi-config.js
```

```
static BaseURL = "https://subdomain.domain.net/api";
static StorageBaseURL = "https://subdomain.domain.net";
static TwitchClientID = "{TWITCH_CLIENT_ID}";
static TwitchAuthURL = "{TWITCH_AUTH_URL}";
static LocalhostRedirectURL = "{TWITCH_REDIRECT_URL}";
```

## Deploy Frontend and Backend
To deploy backend django application.
```
sed -e "s/\$PROJECT/<project-id>/g" \
  -e "s/\$VERSION/<version>/g" \
  django/django.yaml | kubectl apply -f -
```
To deploy celery application.
```
sed -e "s/\$PROJECT/<project-id>/g" \
  -e "s/\$VERSION/<version>/g" \
  celery/celery.yaml | kubectl apply -f -
```
To deploy frontend react application.
```
sed -e "s/\$PROJECT/<project-id>/g" \
  -e "s/\$VERSION/<version>/g" \
  frontend/frontend.yaml | kubectl apply -f -
```
**`project-id`** is the GCP project id that contains the docker image in GCR service.  
**`version`** is the image version to deploy on K8.  

## Deploy ML Components
To deploy ML audio application.
```
sed -e "s/\$PROJECT/<project-id>/g" \
  -e "s/\$VERSION/<version>/g" \
  ml-audio/audio.yaml | kubectl apply -f -
```
To deploy ML overly application.
```
sed -e "s/\$PROJECT/<project-id>/g" \
  -e "s/\$VERSION/<version>/g" \
  ml-overlay/overlay.yaml | kubectl apply -f -
```
To deploy ML underlay application.
```
sed -e "s/\$PROJECT/<project-id>/g" \
  -e "s/\$VERSION/<version>/g" \
  ml-underlay/underlay.yaml | kubectl apply -f -
```
**`project-id`** is the GCP project id that contains the docker image in GCR service.  
**`version`** is the image version to deploy on K8.  

## Deploy CronJobs
To deploy the cronjobs pertaining to django on K8 cluster, run the following.
```
sed -e "s/\$PROJECT/<project-id>/g" \
  -e "s/\$VERSION/<version>/g" \
  django/jobs.yaml | kubectl apply -f -
```

# Application Ingress
Using ingress resource of K8, you can manage how the traffic is routed into your applications from outside the cluster. To enable HTTPS for ingress on K8, you must make use of SSL certificate resource on K8 first.  

## SSL Certificates for HTTPS
Deploy managed SSL certificate resource on K8 cluster.
```
sed -e "s/\$DOMAIN/<domain>/g" ingress/cert.yaml | kubectl apply -f -
```
**`domain`** is the domain name with no wildcards that you want to issue SSL certificate against.  

> **Note:** An SSL cert resource on K8 can have only one domain. To have SSL for multiple domains, issue SSL cert for each domain using above command and replace the `$DOMAIN` with actual value.  
  
Get the list of SSL cert resources on K8 cluster.
```
kubectl get managedcertificate
```
Output should be similar to this.
```
NAME                      AGE
example.mysite.com        8s
```

### SSL Certificate Status
To get the status of certificate resource on K8 cluster.
```
kubectl get managedcertificate <cert-name> \
  -o jsonpath="{.status.certificateStatus}"
```
**`cert-name`** is the SSL certificate name on K8 cluster.  

> **Note:** SSL cert on K8 cluster is activated by GCP automatically only if it is attached to an `Ingress` resource that has a external IP address and the domain used in SSL cert points towards the same external IP address.
  
## Static IP for Ingress
Create a static IP address to use in ingress resource of K8.
```
gcloud compute addresses create <address-name> --global
```
  
To delete the static IP address, you can run the following command.
```
gcloud compute addresses delete <address-name> --global
```
**`address-name`** is a human readable name for the IP address.  

> **Note:** Google charges you for each external IP address that you reserve but not use. If you are sure that you don't need the external IP address anymore, delete it to release it back to the IPv4 pool.
  
## Deploy Ingress
To deploy `Ingress` resource on K8 cluster, use the following command.
```
sed -e "s/\$ADDRESS/<address>/g" -e "s/\$DOMAINS/<domains>/g" \
  -e "s/\$HOST/<host>/g" ingress/main.yaml | kubectl apply -f -
```
**`address`** is the human readable name for the IP address created in previous step.  
**`domains`** is the comma-separated list of SSL cert names you created in previous steps and want to attach with ingress.  
**`host`** is the name you want to access the ingress from.  

> **Note:** SSL certs created on K8 cluster will be activated by GCP only if they are attached to an `Ingress` resource on K8 cluster. In this case, only the certs in the comma separated list of `domains` will be activated in 5-10 mins by GCP.  

## Ingress Scenario
Consider that you want to achieve the following.  
  
1. Access application from test1.mysite.com and test2.mysite.com  
2. Have HTTPS for both domains above.  

### Ingress Implementation
  
Create SSL cert resources on K8 cluster.  
```
sed -e "s/\$DOMAIN/test1.mysite.com/g" ingress/cert.yaml | kubectl apply -f -
sed -e "s/\$DOMAIN/test2.mysite.com/g" ingress/cert.yaml | kubectl apply -f -
```  
Check if SSL cert resources have been created on K8.  
```
kubectl get managedcertificates

NAME                      AGE
test1.mysite.com          20s
test2.mysite.com          11s
```  
Create a static IP address on GCP.  
```
gcloud compute addresses create k8-ingress --global
```  
Check the IP address issued by GCP.  
```
gcloud compute addresses list --global

NAME          ADDRESS/RANGE  TYPE       NETWORK  REGION  SUBNET  STATUS
k8-ingress    11.111.11.11   EXTERNAL                                    
```  
Create ingress resource on K8 cluster and attach the GCP address and SSL certs to it.  
```
sed -e "s/\$ADDRESS/k8-ingress/g" \
  -e "s/\$DOMAINS/test1.mysite.com,test2.mysite.com/g" \
  -e "s/\$HOST/*.mysite.com/g" main.yaml | kubectl apply -f -
```  
Check the ingress resource on K8 to ensure host and IP address is correct.  
```
kubectl get ingress

NAME   HOSTS          ADDRESS        PORTS   AGE
main   *.mysite.com   11.111.11.11   80      2m
```  
Go to your DNS provider and add `A` records as follows.  
```
Record Name            Record Value
test1.mysite.com       11.111.111.111
test2.mysite.com       11.111.111.111
```  
Wait for SSL certs for both endpoints to be active. If you have followed the above steps correctly, SSL certs should become active by GCP in 5-10 mins.  
```
kubectl get managedcertificate test1.mysite.com \
  -o jsonpath="{.status.certificateStatus}"
kubectl get managedcertificate test2.mysite.com \
  -o jsonpath="{.status.certificateStatus}"
```  
Once both the SSL certs are active, you can visit the application from browser using.  
  
- https://test1.mysite.com  
- https://test2.mysite.com  

