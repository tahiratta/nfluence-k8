#/bin/sh -x
log() {
  echo "$(date '+%d-%b-%Y %T') $1"
}

if [[ -z $BUCKET_PATH ]]; then
  log "BUCKET_PATH not specified."
  exit 1
elif [[ $BUCKET_PATH != gs://* ]]; then
  log "BUCKET_PATH not a valid GCS path: $BUCKET_PATH"
  exit 1
elif [[ -z $TARGET_DIR ]]; then
  log "TARGET_DIR not specified."
  exit 1
else
  bucketName=$(cut -d '/' -f3 <<< $BUCKET_PATH)
  dirPath=${BUCKET_PATH#*//*/}
  if [[ -z $bucketName ]]; then
    log "Could not extract bucketName from BUCKET_PATH: $BUCKET_PATH"
    exit 1
  elif [[ -z $dirPath ]]; then
    log "Could not extract dirPath from BUCKET_PATH: $BUCKET_PATH"
    exit 1
  fi
fi

log "Using target dir: $TARGET_DIR"
mkdir -p $TARGET_DIR/

if [[ $1 == startUp ]]; then
  log "Mounting GCS bucket on [$TARGET_DIR/]..."
  gcsfuse -o nonempty --debug_gcs --debug_fuse --foreground --only-dir \
    $dirPath $bucketName $TARGET_DIR/ > /var/log/gcsfuse/fuse.log 2>&1 & 
elif [[ $1 == stopDown ]]; then
  log "Unmounting GCS bucket from [$TARGET_DIR/]..."
  fusermount -u $TARGET_DIR/
fi
