# enable APIs.
enableAPIs() {
  echo -e "\nEnabling container and cloudbuild apis..."
  gcloud services enable container.googleapis.com
  gcloud services enable cloudbuild.googleapis.com
}

# create cluster.
createCluster() {
  echo ""

  params=""
  if [[ ! -z $zoneName ]]; then
    params="${params} --zone $zoneName"
    targetRegion="${zoneName%-*}"
  elif [[ ! -z $regionName ]]; then
    params="${params} --region $regionName"
    targetRegion="$regionName"
  else
    echo "zoneName or regionName must be specified."
    exit 1
  fi

  cidr=$(echo $(curl api.ipify.io -sS)/32)
  if [[ -z $cidr ]]; then
    echo "Could not detect CIDR for your external IP address.
      You can manually specify CIDRs to whitelist using
      CIDRS=<cidr1>,<cidr2> in command line."
    exit 1
  else
    if [[ -z $CIDRS ]]; then
      CIDRS="$cidr"
    else
      CIDRS="${CIDRS},$cidr"
    fi
  fi

  if [[ -z $machineType ]]; then machineType="n1-standard-1"; fi
  if [[ -z $min ]]; then min=1; fi
  if [[ -z $max ]]; then max=1; fi

  CIDRS=${CIDRS#*,}

  echo "Fetching list of master IPv4 CIDR of K8 clusters in network."
  masterIPs=""
  clusters=$(gcloud container clusters list --uri)
  for entry in $clusters; do
    location=$(cut -d '/' -f8 <<< $entry)
    clusterName=$(basename $entry)

    count=${location//[^-]}
    isRegion="false"

    if [[ ${#count} == 1 ]]; then
      ipv4=$(gcloud container clusters describe $clusterName \
        --format 'get(privateClusterConfig.masterIpv4CidrBlock)' \
        --region $location)
    else
      ipv4=$(gcloud container clusters describe $clusterName \
        --format 'get(privateClusterConfig.masterIpv4CidrBlock)' \
        --zone $location)
    fi
    masterIPs="${masterIPs},$ipv4"
  done
  masterIPs=${masterIPs#*,}
  echo "  Reserved Master CIDR: [$masterIPs]"

  newMasterCIDR="172.16.0.0/28"
  for prefix in {0..100}; do
    newMasterCIDR="172.16.$prefix.0/28"
    isTaken="false"
    for range in ${masterIPs//,/ }; do
      if [[ $range == $newMasterCIDR ]]; then isTaken="true"; fi
    done
    if [[ $isTaken == false ]]; then break; fi
  done

  echo "  New Master CIDR: [$newMasterCIDR]"
  echo -e "\nCreating cluster [$name] with whitelisted CIDRS [$CIDRS]..."
  gcloud container clusters create $name --enable-autorepair --enable-autoupgrade \
    --enable-stackdriver-kubernetes --machine-type $machineType \
    --disk-type pd-ssd --disk-size 50G --enable-ip-alias \
    --enable-private-nodes --enable-master-authorized-networks \
    --master-ipv4-cidr $newMasterCIDR --subnetwork default \
    --maintenance-window-start 2000-01-01T22:00:00Z \
    --maintenance-window-end 2000-01-02T04:00:00Z \
    --maintenance-window-recurrence 'FREQ=WEEKLY;BYDAY=SA,SU' \
    --enable-autoscaling --min-nodes $min --max-nodes $max \
    --num-nodes 1 $params --master-authorized-networks $CIDRS

  if [[ $? == 0 ]]; then name=gke-$name bash "$0" addSchedule; fi
}

updateCluster() {
  echo ""

  params=""
  if [[ ! -z $zoneName ]]; then
    params="${params} --zone $zoneName"
  elif [[ ! -z $regionName ]]; then
    params="${params} --region $regionName"
  else
    echo "zoneName or regionName must be specified."
    exit 1
  fi

  if [[ ! -z $CIDRS ]]; then :;
  else
    cidr=$(echo $(curl api.ipify.io -sS)/32)
    if [[ ! -z $cidr ]]; then
      CIDRS="${CIDRS},$cidr"
    else
      echo "Could not detect CIDR for your external IP address.
        You can manually specify CIDRs to whitelist using
        CIDRS=<cidr1>,<cidr2> in command line."
      exit 1
    fi
  fi

  CIDRS=${CIDRS#*,}
  echo "Updating cluster [$name] with whitelisted CIDRS [$CIDRS]..."
  gcloud container clusters update $name $params \
    --enable-master-authorized-networks --master-authorized-networks $CIDRS

  if [[ ! -z $max ]]; then
    params="${params} --max-nodes $max"
  fi
  if [[ ! -z $min ]]; then
    params="${params} --min-nodes $min"
  fi
  if [[ ! -z $max || ! -z $min ]]; then
    params="--enable-autoscaling ${params}"
    echo "Updating cluster [$name] with params [$params]..."
    gcloud container clusters update $name $params
  fi
}

addSnapshotSchedule() {

  if [[ -z $regionName && -z $zoneName ]]; then
    echo "regionName or zoneName must be specified."
    exit 1
  fi

  if [[ ! -z $regionName ]]; then :;
  else regionName="$(cut -d'-' -f1 <<< $zoneName)-$(cut -d'-' -f2 <<< $zoneName)"; fi

  scheduleName='daily-schedule'
  location=$(cut -d'-' -f1 <<< $regionName)

  echo "schedule ($scheduleName)"
  echo -e "location ($location)\n"

  echo "Checking if policy [$scheduleName] exists in region [$regionName]..."
  schedule=$(gcloud compute resource-policies list \
    --filter NAME=$scheduleName | grep "$regionName" | cut -d ' ' -f1)

  if [[ -z $schedule ]]; then
    echo "Creating snapshot schedule..."
    gcloud compute resource-policies create snapshot-schedule \
      daily-schedule --daily-schedule --start-time "22:00" \
      --storage-location $location --max-retention-days 14 --region $regionName
  else
    echo "Snapshot schedule already present."
  fi

  disks=$(gcloud compute disks list --uri | grep disks/$name-)
  for disk in $disks; do
    diskZone=$(cut -d '/' -f9 <<< $disk)
    diskName=$(basename $disk)

    echo "Adding snapshot policy to disk [$diskName][$diskZone]..."
    gcloud compute disks add-resource-policies $diskName \
      --resource-policies daily-schedule --zone $diskZone
  done
}

# configure kubectl for the cluster.
configureKubectl() {
  echo ""

  params=""
  if [[ ! -z $zoneName ]]; then
    params="${params} --zone $zoneName"
  elif [[ ! -z $regionName ]]; then
    params="${params} --region $regionName"
  else
    echo "zoneName or regionName must be specified."
    exit 1
  fi

  echo "Fetching cluster credentials..."
  gcloud container clusters get-credentials $name $params
}

SCRIPTPATH="$(cd "$(dirname "$0")"; pwd -P)"

if [[ -z $name && ( $1 == buildImage || $1 == createHostKeys ) ]]; then :;
elif [[ ! -z $name ]]; then echo "name ($name)";
else echo "name must be specified."; exit 1; fi

if [[ $1 == createCluster ]]; then createCluster;
elif [[ $1 == updateCluster ]]; then updateCluster;
elif [[ $1 == addSchedule ]]; then addSnapshotSchedule;
elif [[ $1 == setKube ]]; then configureKubectl;
fi
